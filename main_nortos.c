#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <NoRTOS.h>

#include <ti/devices/cc32xx/inc/hw_memmap.h>
#include <ti/devices/cc32xx/inc/hw_types.h>
#include <ti/devices/cc32xx/driverlib/prcm.h>
#include <ti/devices/cc32xx/driverlib/i2s.h>
#include <ti/drivers/I2C.h>

#include "Board.h"
#include "config.h"
#include "fdacoefs.h"

// You can select the sample rate here:
#define SAMPLINGFREQUENCY 8000
#if SAMPLINGFREQUENCY < 8000 || SAMPLINGFREQUENCY > 48000 || SAMPLINGFREQUENCY % 4000 != 0
#error Sampling Frequency must be between 8 kHz and 48 kHz (included) and must be a multiple of 4 kHz.
#endif

static unsigned long dataLeft, dataRight, outputSample;
static int k, n = 0;
static int N = 21;

typedef struct
{
    int write;
    int read;
    int max;
    int stored;
    int16_t *buffer[BL];
} CircularBuffer;

CircularBuffer *buffer;

CircularBuffer * createBuffer()
{
    CircularBuffer *cBuffer = malloc(sizeof(CircularBuffer));
    cBuffer->write = 2;
    cBuffer->read = 2;
    cBuffer->max = BL;
    cBuffer->stored = 0;

    int i;
    for(i = 0; i < BL; i++)
    {
        cBuffer->buffer[i] = 0;
    }

    return cBuffer;
}

void writeToBuffer(CircularBuffer *cbuffer, int16_t value)
{
    if(cbuffer->write == cbuffer->read && cbuffer->stored == cbuffer->max)
    {
       cbuffer->read = (cbuffer->read + 1) % BL;
    }

    cbuffer->buffer[cbuffer->write] = &value;
    cbuffer->write = (cbuffer->write + 1) % BL;

    if(cbuffer->stored < cbuffer->max)
    {
        cbuffer->stored++;
    }
}

int readFromBuffer(CircularBuffer *cbuffer)
{
    if(cbuffer->stored == 0)
    {
       return 0;
    }

    int value = *cbuffer->buffer[cbuffer->read];
    cbuffer->read = (cbuffer->read + 1) % BL;
    cbuffer->stored--;
    return value;
}

void I2S_ISR(void)
{
    if (n == 0)
    {
        I2SDataGetNonBlocking(I2S_BASE, I2S_DATA_LINE_1, &dataLeft);
        writeToBuffer(buffer, dataLeft);
        outputSample = 0;
        k = 0;

        // write left channel
        while (k < N) {
               int temp = readFromBuffer(buffer);
               outputSample = outputSample + (B[k] * temp);
               writeToBuffer(buffer, temp);
               k++;
           }

        I2SDataPutNonBlocking(I2S_BASE, I2S_DATA_LINE_0, outputSample >> 15);

    }
    else
    {
        I2SDataGetNonBlocking(I2S_BASE, I2S_DATA_LINE_1, &dataRight);
        // write right channel
        I2SDataPutNonBlocking(I2S_BASE, I2S_DATA_LINE_0, dataRight);
    }
    n = (n + 1) % 2;



    I2SIntClear(I2S_BASE, I2S_INT_XDATA);
    //I2SDataPutNonBlocking(I2S_BASE, I2S_DATA_LINE_0, dataLeft);
    //clear ISR, Transmit data ready interrupt enable bit reset

}

int main(void)
{
    // Init CC3220S LAUNCHXL board.
    Board_initGeneral();
    // Prepare to use TI drivers without operating system
    NoRTOS_start();
    // Configure an I2C connection which is used to configure the audio codec.
    I2C_Handle i2cHandle = ConfigureI2C(Board_I2C0, I2C_400kHz);
    // Configure the audio codec.
    ConfigureAudioCodec(i2cHandle, SAMPLINGFREQUENCY);
    // Configure an I2S connection which is use to send/receive samples to/from the codec.
    ConfigureI2S(PRCM_I2S, I2S_BASE, SAMPLINGFREQUENCY);
    // Create circular buffer
    buffer = createBuffer();

    // Register I2S_ISR
    I2SIntRegister(I2S_BASE, I2S_ISR);
    // Enable interrupt to I2S_ISR when I2S is ready to send a sample to the codec
    I2SIntEnable(I2S_BASE, I2S_INT_XDATA);

    while (1);
}
